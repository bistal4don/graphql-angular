import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component'

//Added imports
import { HttpClientModule } from '@angular/common/http'
import { ApolloModule, Apollo } from 'apollo-angular'
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    ApolloModule, 
    HttpLinkModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 

  //To inject the Apollo and the HttpLink we have to add it to the constructor
  constructor(apollo: Apollo, 
    httpLink: HttpLink) {
      apollo.create({
        link: httpLink.create({ uri: 'https://87zn97r5vq.lp.gql.zone/graphql'}),
        cache: new InMemoryCache()
      });  
    }
}
